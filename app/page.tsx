import { GPTForm } from "@/components/form/GPTForm";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center p-10">
      
      <GPTForm />
    </main>
  );
}
