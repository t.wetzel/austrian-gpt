"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { set, useForm } from "react-hook-form";
import { callApi } from "@/gpt/openAPInstance";
import { Textarea } from "../ui/textarea";
import { useState } from "react";
import { ChatCompletionMessageParam } from "openai/resources/chat/index.mjs";
import { Card, CardContent } from "../ui/card";

const formSchema = z.object({
  text: z.string().min(1, {
    message: "The text cannot be empty",
  }),
});

export function GPTForm() {
  const [response, setResponse] = useState<ChatCompletionMessageParam[]>([]);
  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      text: "",
    },
  });

  // 2. Define a submit handler.
  async function onSubmit(values: z.infer<typeof formSchema>) {
    const response = await callApi(values.text);
    setResponse(response);
  }

  return (
    <div className="w-1/2">
      <div className="space-y-4">
        {response.map((r, i) => (
          <Card
            key={i}
            className={
              r.role === "user"
                ? "bg-primary text-primary-foreground"
                : "bg-secondary text-secondary-foreground"
            }
          >
            <CardContent className="p-5">{r.content}</CardContent>
          </Card>
        ))}
      </div>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="space-y-8"
        >
          <FormField
            control={form.control}
            name="text"
            render={({ field }) => (
              <FormItem>
                <FormLabel>What do you have to say?</FormLabel>
                <FormControl>
                  <Textarea {...field} />
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />
          <Button type="submit">Submit</Button>
        </form>
      </Form>
    </div>
  );
}
