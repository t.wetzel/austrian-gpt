"use client";

import {
  NavigationMenu,
  NavigationMenuContent,
  NavigationMenuIndicator,
  NavigationMenuItem,
  NavigationMenuLink,
  NavigationMenuList,
  NavigationMenuTrigger,
  NavigationMenuViewport,
  navigationMenuTriggerStyle,
} from "@/components/ui/navigation-menu";
import Link from "next/link";
import { ModeToggle } from "../theme/mode-toggle";

interface menuProps {}

export const Menu: React.FC<menuProps> = ({}) => {
  return (
    <NavigationMenu className="max-h-20 w-full min-w-full sticky top-0 p-10">
      <NavigationMenuList>
        <NavigationMenuItem className="text-3xl font-bold bg-secondary rounded-xl p-2">Austrian GPT</NavigationMenuItem>
        <div className="ml-auto">
          <ModeToggle />
        </div>
      </NavigationMenuList>
    </NavigationMenu>
  );
};
