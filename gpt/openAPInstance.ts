"use server";

import OpenAI from "openai";

const openaiInstance = new OpenAI({});

const messages: OpenAI.Chat.ChatCompletionCreateParams["messages"] = [];

const functions: { [key: string]: Function } = {
  yodelForMe: function () {
    return "🥨 layeeeee layeeeee layeeeee hooooo hoohooooo leeee hooooo hiahooodel heeeeeeeee 🥨";
  },
  getBestFood: function () {
    return "Kaiserschmarrn 🇦🇹";
  },
  getBestActor: function () {
    return "Arnold Schwarzenegger 💪🏼";
  },
  getNumberMultiplication: function (args: any) {
    const { number1, number2 } = args;
    return `Sans ${(number1 - 1) * number2}? Na, des san ${number1 * number2}`;
  },
};

// create types (infer based on stream)
export const getCompletion = async (
  messages: OpenAI.Chat.ChatCompletionCreateParams["messages"]
) => {
  const params: OpenAI.Chat.ChatCompletionCreateParams = {
    messages: messages,
    model: "gpt-3.5-turbo",
    function_call: "auto",
    functions: [
      {
        name: "yodelForMe",
        parameters: { type: "object", properties: {} },
        description: "yodels for you",
      },
      {
        name: "getBestFood",
        parameters: { type: "object", properties: {} },
        description: "recommends the best food",
      },
      {
        name: "getBestActor",
        parameters: { type: "object", properties: {} },
        description: "recommends the best actor",
      },
      {
        name: "getNumberMultiplication",
        parameters: {
          type: "object",
          properties: {
            number1: { type: "number", description: "the first number that should be multiplied" },
            number2: { type: "number", description: "the second number that should be multiplied" },
          },
          required: ["number1", "number2"],
        },
        description: "multiplies two numbers",
      },
    ],
  };
  const response = await openaiInstance.chat.completions.create(params);

  return response;
};

export async function callApi(content: string) {
  console.log("this is the content:", content);
  const role = "user" as const;
  const message = {
    role,
    content,
  };
  messages.push(message);
  let response = await getCompletion(messages);

  if (
    response.choices[0].finish_reason === "function_call" &&
    response.choices[0].message.function_call
  ) {
    const fnName = response.choices[0].message.function_call.name;
    const args = JSON.parse(response.choices[0].message.function_call?.arguments);

    console.log(args);

    // call the function
    const fn = functions[fnName];
    const result = fn(args);

    messages.push({
      role: "function" as const,
      name: fnName,
      content: result,
    });

    return messages;
  }

  //   add the response to the messages
  messages.push({
    role: "assistant",
    content: response.choices[0].message.content,
  });
  return messages;
}
