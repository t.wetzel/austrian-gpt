## Setup

add `.env.local` with the content: `OPENAI_API_KEY = enter_your_api_key_here`

## Example Usage

![Example Image](public/screenshot.png)
